import App from '../App'

import Vue from 'vue'
import Router from 'vue-router'
import taskDetail from '../views/home/children/taskDetail'
const home = r => require.ensure([], () => r(require('../views/home/home')), 'home')
// const taskDetail = r => require.ensure([], () => r(require('../views/home/taskDetail')), 'taskDetail')

Vue.use(Router)

export default new Router({
  mode: 'hash',
  routes: [
    {
      path: '/',
      // name: '主页',
      component: App,
      children: [
        {
          path: '',
          redirect: '/home'
        },
        {
          path: '/home',
          component: home,
          children: [{
            path: 'taskDetail', // 食品详情页
            component: taskDetail
          }]
        }
        // ,
        // {
        //   path: '/home/taskDetail',
        //   component: taskDetail
        // }
      ]
    }
  ]
})
