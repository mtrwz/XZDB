import fetch from 'src/utils/fetch2'

export function getDDUserInfo (code) {
  return fetch('/userinfo?code=' + code)
};

export function getUser () {
  return fetch({
    method: 'GET',
    url: '/mob/login/user/getUser'
  })
};

export function getUtList (userCode) {
  return fetch('/mob/ut/self/list?user_code=' + userCode)
}

export function getXZDBList (query) {
  return fetch({
    method: 'post',
    url: '/mob/ut/self/xzdblist',
    data: query
  })
}

export function updateUt (query) {
  return fetch({
    method: 'post',
    url: '/mob/ut/self/update',
    data: query
  })
}

export function getUtDetail (query) {
  return fetch({
    method: 'post',
    url: '/mob/ut/self/detail',
    data: query
  })
}
