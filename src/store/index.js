import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
const store = new Vuex.Store({
  state: {
    user: null,
    isUpdate: false
  },
  mutations: {
    setUser (state, user) {
      state.user = user
    },
    setIsUpdate (state, isUpdate) {
      state.isUpdate = isUpdate
    }
  }
})

export default store
